# Copyright (C) 2024 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)
#
# use-makefile-lib-so/use-libfoo-so-make_1.2.3.bb uses the library
# 
# Demonstrate how not to dish out sources, but only 
# the library + header file
# This recipe uses only the relevant packages via the bin_package class
#
# You need to copy over the packages of libfoo-so-make over from ${WORKDIR}
# libfoo1_1.2.3-r0.5_armv7at2hf-neon.ipk
# libfoo1_1.2.3-r0.5_armv7at2hf-neon.ipk
# we don't provide the -dbg package, which would contain the sources

DESCRIPTION = "libfoo.so.1.2.3 with makefile from ipk"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PR="r0.5"

# libfoo-dbg_1.2.3-r0.5_armv7at2hf-neon.ipk
SRC_URI = "file://libfoo1_1.2.3-${PR}_armv7at2hf-neon.ipk;subdir=${BP}"
SRC_URI += "file://libfoo-dev_1.2.3-${PR}_armv7at2hf-neon.ipk;subdir=${BP}"

inherit bin_package

# Something like this is what we want:

# + oe-pkgdata-util list-pkg-files libfoo-so-make
# libfoo-so-make:
#         /usr/lib/libfoo.so.1     (symlink) to /usr/lib/libfoo.so.1.2.3
#         /usr/lib/libfoo.so.1.2.3 (real file)

# + oe-pkgdata-util list-pkg-files libfoo-so-make-dev
# libfoo-so-make-dev:
#         /usr/include/foo.h       (real file)
#         /usr/lib/libfoo.so       (symlink) to /usr/lib/libfoo.so.1.2.3

INSANE_SKIP:${PN} = "already-stripped"

# make sure to get rid of everything from libfoo-so-make
# since sources would be in the -dbg package of it
do_install[depends] += "libfoo-so-make:do_cleanall"
