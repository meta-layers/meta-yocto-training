DESCRIPTION = "Simple helloworld make application"
SECTION = "examples"
SUMMARY = "Simple helloworld make application"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "e6ca74f9bc0d5d3f790b217d02b9107d49e56145"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/hello-makefile.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

EXTRA_OEMAKE = "DESTDIR=${D}${bindir}"

do_install() {
        install -d ${D}${bindir}
        oe_runmake install
}
