DESCRIPTION = "Simple helloworld cmake application using pthreads"
SECTION = "examples"
SUMMARY = "Simple helloworld cmake application using pthreads"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "335cc3e0c1bd3be74aa296a2a499112287061083"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/helloworld-pthread-cmake.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE = ""
