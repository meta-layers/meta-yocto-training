SECTION = "examples"
SUMMARY = "Simple python3 helloworld application - using poetry"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"

SRC_URI = "git://github.com/RobertBerger/python-poetry-helloworld;protocol=https;branch=main"

# Modify these as desired
#PV = "1.0+git"
SRCREV = "b37518f3c9c889d964a47a6083f43328236c5428"

S = "${WORKDIR}/git"

inherit python_poetry_core

RDEPENDS:${PN} += "python3"
RDEPENDS:${PN} += "python3-colorama"

DEPENDS += "python3-colorama"

