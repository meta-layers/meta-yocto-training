# Copyright (C) 2024 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)
#
# Demonstrate how to either
#  use a dependency, which contains the -dbg package (sources)
#  .which comes in handy for debugging while we develop the library
#
#  or a dependency, which does not contain the -dbg package

DESCRIPTION = "use libfoo.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/use-makefile-lib-so.git;protocol=https;branch=master" 
SRCREV = "c04c296620c8e255f27c1de2e4a56643c465ef36"
#SRCREV = "${AUTOREV}"
#PV = "1.2.3+git${SRCPV}"

# -->
# with this package this we get .h and .so files plus symlinks and sources,
# if we include the -dev package
#DEPENDS = "libfoo-so-make"
# <--

# -->
# with this package this we only get .h and .so files plus symlinks
# no sources
DEPENDS = "libfoo-so-make-bin"
# <--

# this is just for the package manager
# so libfoo-so-make is installed automatically
# if you install this testcase
# ... but due to DEPENDS it should add the RDEPENDS automatically
# RDEPENDS:${PN} = "libfoo-so-make"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass this to my Makefile
EXTRA_OEMAKE = "DESTDIR=${D}${bindir}"

# all the work is done in the Makefile
# just creating dir and calling make install
do_install() {
        # create the dir if it does not already exists
        install -d ${D}${bindir}
        # install test case in there
        oe_runmake install
}

# just as a precaution against multi-core and crappy Makefiles
PARALLEL_MAKE=""

# Something like this is what we want:
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make
# use-libfoo-so-make:
#        /usr/bin/foo_test
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-dev
# use-libfoo-so-make-dev:
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-dbg
# use-libfoo-so-make-dbg:
#        /usr/lib/debug/usr/bin/foo_test.debug
#        /usr/src/debug/use-libfoo-so-make/1.2.3+gitAUTOINC+5361aa56b7-r0/git/foo_test.c
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-src
# use-libfoo-so-make-src:

