DESCRIPTION = "Simple hello world in Python 3 with setuptools3"
SECTION = "examples"
SUMMARY = "Simple hello world in Python 3 with setuptools3"
HOMEPAGE = "https://github.com/RobertBerger/python-examples/tree/master/hello-python3"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "669aacab1ccca4c3c3e5f226a0fa17eadcbeece5"
#SRCREV = "${AUTOREV}"
#PV = "1.0+git${SRCPV}"
# upstream:
SRC_URI = "git://github.com/RobertBerger/python-examples;protocol=https;branch=master"

S = "${WORKDIR}/git/${BPN}"

inherit setuptools3

RDEPENDS:${PN} += "python3-core"
