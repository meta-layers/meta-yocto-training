DESCRIPTION = "bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
#OVERRIDES:prepend = "sun:rain:snow:"
OVERRIDES:append = ":sun:rain:snow"
OVERRIDES:append = ":${OTHER}"
OTHER = "hail"
VAR_PROTECTION_t43:rain = "umbrella"
VAR_PROTECTION_t43:hail = "duck"

python do_p76_4_py () {
  overrides = d.getVar("OVERRIDES", True)
  var_protection_t43 = d.getVar("VAR_PROTECTION_t43", True)
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_PROTECTION_t43 is %s\n" %var_protection_t43)
}

addtask do_p76_4_py
# <--

