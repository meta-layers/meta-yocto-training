DESCRIPTION = "bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
#OVERRIDES:prepend = "left:middle:right:"
OVERRIDES:append = ":left:middle:right"
VAR_A_t95 = "1_t95 2_t95"
VAR_A_t95:left = "3_t95"

#OVERRIDES:append = ":left:middle:right"

python do_conditional_oskar_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t95 = d.getVar("VAR_A_t95", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t95 is %s\n" %var1_t95)
}

addtask do_conditional_oskar_py
# <--

