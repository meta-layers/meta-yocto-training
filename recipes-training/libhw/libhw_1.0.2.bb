DESCRIPTION = "Autotooled lib"
SUMMARY = "Autotooled lib"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
SECTION = "examples"
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = " virtual/gettext"

SRCREV = "d5c01e4872fa61ee7339a69aac32d0857e4ba8a2"
SRC_URI = "git://gitlab.com/exempli-gratia/${BPN}.git;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
#PV = "1.0.2+git${SRCPV}"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

inherit autotools
