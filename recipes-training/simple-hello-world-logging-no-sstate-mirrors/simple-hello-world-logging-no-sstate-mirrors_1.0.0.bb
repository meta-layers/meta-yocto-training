DESCRIPTION = "Simple helloworld application - no sstate mirrors"
SECTION = "examples"
SUMMARY = "no sstate mirrors"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "bccb67172fd736e23c08b1aca69f63d0e51fb656"
#SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/exempli-gratia/simple-hello-world-logging-no-sstate-mirrors.git;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
# https://stackoverflow.com/questions/55542738/yocto-ci-build-number-pr-service-do-not-increment-pr
# https://docs.windriver.com/bundle/Wind_River_Linux_Toolchain_and_Build_System_Users_Guide_LTS_19/page/mmo1403548674799.html
#PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

# if we use SSTATE_MIRRORS 
# cleansstate followed by bitbale simple-hello-world-logging
# does nothing

# What if we don't use SSTATE_MIRRORS?
SSTATE_MIRRORS=""

# let's try [nostamp]
# do_compile[nostamp] = "1"

# inherit logging
# This class is enabled by default since it is inherited by the base class.

do_compile() {
        ${CC} -Werror simple-hello-world-logging-no-sstate-mirrors.c ${CFLAGS} ${LDFLAGS} -o simple-hello-world-logging-no-sstate-mirrors
         bbdebug 1 "---> Completed do_compile_task <---"
#        bbplain "---> bbplain <---"
#        bbnote  "---> bbnote <---"
#        bbwarn  "---> bbwarn <---"
#        bberror "---> bberror <---"
#        bbfatal "---> bbfatal <---"
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 simple-hello-world-logging-no-sstate-mirrors ${D}${bindir}
}

# Note: added ${LDFLAGS} in morty to get rid of:
#       do_package_qa: QA Issue: No GNU_HASH in the elf binary

