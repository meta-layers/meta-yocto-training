DESCRIPTION = "Autotooled lib"
SECTION = "examples"
SUMMARY = "Autotooled lib"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = " virtual/gettext"

#SRC_URI = "file://${BPN}-${PV}.tar.gz"
SRCREV = "7ae29f8601d1cab21e12a94b42ad8fc1ff177dab"
#SRCREV = "${AUTOREV}"
#PV = "1.0.2+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libhw-autotooled-so.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit autotools
