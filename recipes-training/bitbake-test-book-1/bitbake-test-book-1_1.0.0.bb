DESCRIPTION = "bitbake test p.73/1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
VAR_1_t30 = "This is a \"quotation\" inside a variable assignment"
VAR_2_t30 = "This is a second 'quotation' inside a variable assignment"

python do_p73_1_py () {
  var1_t30 = d.getVar("VAR_1_t30", True)
  var2_t30 = d.getVar("VAR_2_t30", True)
  bb.debug(1, "VAR_1_t30 is %s\n" %var1_t30)
  bb.debug(1, "VAR_2_t30 is %s\n" %var2_t30)
}

addtask do_p73_1_py
# <--

# -->
VAR_A_t31 ?= "value1"
VAR_B_t31 ?= "value2"
VAR_B_t31 ?= "value3"
VAR_C_t31 ?= "value4"
VAR_C_t31 =  "value5"

python do_p73_2_py () {
  var_A_t31 = d.getVar("VAR_A_t31", True)
  var_B_t31 = d.getVar("VAR_B_t31", True)
  var_C_t31 = d.getVar("VAR_C_t31", True)
  bb.debug(1, "VAR_A_t31 is %s\n" %var_A_t31)
  bb.debug(1, "VAR_B_t31 is %s\n" %var_B_t31)
  bb.debug(1, "VAR_C_t31 is %s\n" %var_C_t31)
}

addtask do_p73_2_py
# <--

# -->
VAR_A_t32 ??= "value1"
VAR_B_t32 ??= "value2"
VAR_B_t32 ??= "value3"
VAR_C_t32  ?= "value4"
VAR_C_t32 ??= "value5"
VAR_D_t32   = "value6"
VAR_D_t32 ??= "value7"

python do_p73_3_py () {
  var_A_t32 = d.getVar("VAR_A_t32", True)
  var_B_t32 = d.getVar("VAR_B_t32", True)
  var_C_t32 = d.getVar("VAR_C_t32", True)
  var_D_t32 = d.getVar("VAR_D_t32", True)
  bb.debug(1, "VAR_A_t32 is %s\n" %var_A_t32)
  bb.debug(1, "VAR_B_t32 is %s\n" %var_B_t32)
  bb.debug(1, "VAR_C_t32 is %s\n" %var_C_t32)
  bb.debug(1, "VAR_D_t32 is %s\n" %var_D_t32)
}

addtask do_p73_3_py
# <--

# -->
VAR_1_t33 = "jumps over"
VAR_2_t33 = "The quick brown fox ${VAR_1_t33} the lazy dog."

python do_p73_4_py () {
  var1_t33 = d.getVar("VAR_1_t33", True)
  var2_t33 = d.getVar("VAR_2_t33", True)
  bb.debug(1, "VAR_1_t33 is %s\n" %var1_t33)
  bb.debug(1, "VAR_2_t33 is %s\n" %var2_t33)
}

addtask do_p73_4_py
# <--


# -->
VAR_1_t34  = "jumps over"
VAR_2_t34  = "${VAR_1_t34} the lazy dog."
VAR_1_t34  = "falls on"
VAR_3_t34  = "The rain in Spain ${VAR_1_t34} the plain."
VAR_4_t34 := "The quick brown fox ${VAR2}"

python do_p74_1_py () {
  var1_t34 = d.getVar("VAR_1_t34", True)
  var2_t34 = d.getVar("VAR_2_t34", True)
  var3_t34 = d.getVar("VAR_3_t34", True)
  var4_t34 = d.getVar("VAR_4_t34", True)
  bb.debug(1, "VAR_1_t34 is %s\n" %var1_t34)
  bb.debug(1, "VAR_2_t34 is %s\n" %var2_t34)
  bb.debug(1, "VAR_3_t34 is %s\n" %var3_t34)
  bb.debug(1, "VAR_4_t34 is %s\n" %var4_t34)
}

addtask do_p74_1_py
# <--

# -->
#DATE = "${@time.strftime('%A %B %d,  %Y', time.gettime())}"
DATE = "${@time.strftime('%Y%m%d',time.gmtime())}"
TODAY := "Today is: ${DATE}."

python do_p74_2_py () {
  var1_t35 = d.getVar("DATE", True)
  var2_t35 = d.getVar("TODAY", True)
  bb.debug(1, "DATE is %s\n" %var1_t35)
  bb.debug(1, "TODAY is %s\n" %var2_t35)
}

addtask do_p74_2_py

# <--


# -->
VAR_1_t35   = "12"
VAR_1_t35  += "34"
VAR_2_t35   = "89"
VAR_2_t35  =+ "67"
VAR_3_t35   = "5"
VAR_3_t35  += "${VAR1}"
VAR_3_t35  =+ "${VAR2}"

python do_p74_3_py () {
  var1_t35 = d.getVar("VAR_1_t35", True)
  var2_t35 = d.getVar("VAR_2_t35", True)
  var3_t35 = d.getVar("VAR_3_t35", True)
  bb.debug(1, "VAR_1_t35 is %s\n" %var1_t35)
  bb.debug(1, "VAR_2_t35 is %s\n" %var2_t35)
  bb.debug(1, "VAR_3_t35 is %s\n" %var3_t35)
}

addtask do_p74_3_py
# <--


# -->
VAR_1_t36   = "12"
VAR_1_t36  .= "34"
VAR_2_t36   = "89"
VAR_2_t36  =. "67"
VAR_3_t36   = "5"
VAR_3_t36  .= "${VAR1}"
VAR_3_t36  =. "${VAR2}"

python do_p75_1_py () {
  var1_t36 = d.getVar("VAR_1_t36", True)
  var2_t36 = d.getVar("VAR_2_t36", True)
  var3_t36 = d.getVar("VAR_3_t36", True)
  bb.debug(1, "VAR_1_t36 is %s\n" %var1_t36)
  bb.debug(1, "VAR_2_t36 is %s\n" %var2_t36)
  bb.debug(1, "VAR_3_t36 is %s\n" %var3_t36)
}

addtask do_p75_1_py
# <--

# -->
VAR_1_t37         = "12"
VAR_1_t37:append  = "34"
VAR_2_t37         = "89"
VAR_2_t37:prepend = "67"
VAR_3_t37         = "5"
VAR_3_t37:append  = "${VAR_1_t37}"
VAR_3_t37:prepend = "${VAR_2_t37}"

python do_p75_2_py () {
  var1_t37 = d.getVar("VAR_1_t37", True)
  var2_t37 = d.getVar("VAR_2_t37", True)
  var3_t37 = d.getVar("VAR_3_t37", True)
  bb.debug(1, "VAR_1_t37 is %s\n" %var1_t37)
  bb.debug(1, "VAR_2_t37 is %s\n" %var2_t37)
  bb.debug(1, "VAR_3_t37 is %s\n" %var3_t37)
}

addtask do_p75_2_py
# <--

# -->
VAR_1_t38        = "123 456 789 123456789 789 456 123 123 456"
VAR_1_t38:remove = "123"
VAR_1_t38:remove = "456"

python do_p75_3_py () {
  var1_t38 = d.getVar("VAR_1_t38", True)
  bb.debug(1, "VAR_1_t38 is %s\n" %var1_t38)
}

addtask do_p75_3_py
# <--

