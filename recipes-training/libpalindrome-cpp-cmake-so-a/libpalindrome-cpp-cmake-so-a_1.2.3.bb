DESCRIPTION = ".a, .so library made with cmake"
SECTION = "examples"
SUMMARY = ".a, .so library made with cmake"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "1c4f10041591808c576c3bc0b16de3e5e8f669bd"
#SRCREV = "${AUTOREV}"
#PV = "1.2.3+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libpalindrome-cpp-cmake-so-a.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE += "-DLPALCPP_WITH_STATIC=ON -DLPALCPP_WITH_SHARED=ON"

# we need the empty main pkg, as -staticdev depends on it => they end up in the SDK
# ALLOW_EMPTY:${PN} = "1"
