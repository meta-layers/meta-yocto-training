DESCRIPTION = "Simple helloworld cmake application plus feature"
SECTION = "examples"
SUMMARY = "Simple helloworld cmake application plus feature"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "9c5eb5620716fc651b43c25dd1e1a1917967cea7"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/helloworld-cmake-feature.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit pkgconfig cmake

# enabled by default:
# PACKAGECONFIG ??= "withdb"

# disabled by default:
PACKAGECONFIG ??= ""

# If you want to enable withdb in your .bbappend 
# or through local.conf you need to add withdb 
# to PACKAGECONFIG variable.

# add to local.conf
# DISTRO_FEATURES:append = " db"

# get it from DISTRO_FEATURES:
PACKAGECONFIG ??= "${@bb.utils.contains('DISTRO_FEATURES', 'db', 'withdb', '', d)}"
#                                        |                  |     |         |  |
#                                        |                  |     |         |  - 5
#                                        |                  |     |         ---- 4
#                                        |                  |     -------------- 3
#                                        |                  -------------------- 2
#                                        --------------------------------------- 1

# 1 variable -- the variable name. This will be fetched and expanded (using
#   d.getVar(variable)) and then split into a set().
#
# 2 checkvalues -- if this is a string it is split on whitespace into a set(),
#   otherwise coerced directly into a set().
#
# 3 truevalue -- the value to return if checkvalues is a subset of variable.
# 
# 4 falsevalue -- the value to return if variable is empty or if checkvalues is
#   not a subset of variable.
#
# 5 d -- the data store.


PACKAGECONFIG[withdb] = "-DDATABASE=yes,-DDATABASE=no, ,  "
#                        |              |             |  | 
#                        |              |             |  |
#                        |              |             |  -- 4
#                        |              |             ----- 3
#                        |              ------------------- 2
#                         --------------------------------- 1
#
# 1 Extra arguments that should be added to the configure script 
#   argument list (EXTRA_OECONF or PACKAGECONFIG_CONFARGS) if the feature is enabled.
#
# 2 Extra arguments that should be added to EXTRA_OECONF or PACKAGECONFIG_CONFARGS 
#   if the feature is disabled.
#
# 3 Additional build dependencies (DEPENDS) that should be added if the feature is enabled.
#
# 4 Additional runtime dependencies (RDEPENDS) that should be added if the feature is enabled. 

# ${PACKAGECONFIG_CONFARGS} will automatically be added to EXTRA_OECMAKE 
