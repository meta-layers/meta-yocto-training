DESCRIPTION = "Simple helloworld cmake application, Eclipse Cmake project"
SECTION = "examples"
SUMMARY = "Simple helloworld cmake application, Eclipse Cmake project"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "fdb66d6ccf87600e780ca01a0850c06de5a50735"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/eclipse-cmake.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE = ""

# hack for auto generated Cmake template
do_compile:prepend() {
# cmake.bblass adds:
#       -DCMAKE_INSTALL_PREFIX:PATH=/usr
# from our CMakeLists.txt: 
#       replace usr/bin -> bin
# so we will install into:
#       /usr/bin/HelloCmake
       sed -i 's/RUNTIME DESTINATION usr\/bin/RUNTIME DESTINATION bin/' ${S}/CMakeLists.txt
}
