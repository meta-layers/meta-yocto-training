DESCRIPTION = "A trivial Hello world Go program that uses a library package"
SECTION = "examples"
SUMMARY = "A trivial Hello world Go program that uses a library package"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"

# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707"

SRC_URI = "git://${GO_IMPORT};destsuffix=git/src/${GO_IMPORT};nobranch=1;name=${BPN};protocol=https \
           file://modules.txt \
           "

# Modify these as desired
PV = "1.0+git"
SRCREV = "f653c52bfbeeb3b1e86ae5e0d36f4e7c2f79a7c8"

S = "${WORKDIR}/git"

LICENSE += " & ${GO_MOD_LICENSES}"
require ${PN}/golangexample-licenses.inc
require ${PN}/golangexample-modules.inc
GO_IMPORT = "github.com/robertberger/golangexample"
SRCREV_FORMAT = "${BPN}"

inherit go-vendor

RDEPENDS:${PN} += "go-runtime"
RDEPENDS:${PN}-dev += "bash"
