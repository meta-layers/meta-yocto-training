SRC_URI += "${GO_DEPENDENCIES_SRC_URI}"
GO_DEPENDENCIES_SRC_URI = "\
    ${@go_src_uri('go.googlesource.com/tools','v0.14.0',path='golang.org/x/tools')} \
    ${@go_src_uri('go.googlesource.com/mod','v0.13.0',path='golang.org/x/mod')} \
    ${@go_src_uri('go.googlesource.com/sys','v0.13.0',path='golang.org/x/sys')} \
    ${@go_src_uri('gopkg.in/yaml.v3','v3.0.1')} \
"

# golang.org/x/tools@v0.14.0 => 3f4194ee29d7db9b59757dfff729ef55cf89661c
SRCREV_golang.org.x.tools = "3f4194ee29d7db9b59757dfff729ef55cf89661c"
# golang.org/x/mod@v0.13.0 => 5b692803cf76a65fc5d39178c0a36678e69c0e5a
SRCREV_golang.org.x.mod = "5b692803cf76a65fc5d39178c0a36678e69c0e5a"
# golang.org/x/sys@v0.13.0 => 2964e1e4b1dbd55a8ac69a4c9e3004a8038515b6
SRCREV_golang.org.x.sys = "2964e1e4b1dbd55a8ac69a4c9e3004a8038515b6"
# gopkg.in/yaml.v3@v3.0.1 => f6f7691b1fdeb513f56608cd2c32c51f8194bf51
SRCREV_gopkg.in.yaml.v3 = "f6f7691b1fdeb513f56608cd2c32c51f8194bf51"
