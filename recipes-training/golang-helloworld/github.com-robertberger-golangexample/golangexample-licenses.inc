GO_MOD_LICENSES = "BSD-3-Clause"

LIC_FILES_CHKSUM  += "${VENDORED_LIC_FILES_CHKSUM}"
VENDORED_LIC_FILES_CHKSUM = "\
    file://src/${GO_IMPORT}/vendor/golang.org/x/mod/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
    file://src/${GO_IMPORT}/vendor/golang.org/x/sys/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
    file://src/${GO_IMPORT}/vendor/golang.org/x/tools/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
    file://src/${GO_IMPORT}/vendor/gopkg.in/yaml.v3/LICENSE;md5=3c91c17266710e16afdbb2b6d15c761c \
"

# golang.org/x/mod/LICENSE:   BSD 3-Clause "New" or "Revised" License
# golang.org/x/sys/LICENSE:   BSD 3-Clause "New" or "Revised" License
# golang.org/x/tools/LICENSE: BSD 3-Clause "New" or "Revised" License
# gopkg.in/yaml.v3/LICENSE:   BSD 3-Clause "New" or "Revised" License

