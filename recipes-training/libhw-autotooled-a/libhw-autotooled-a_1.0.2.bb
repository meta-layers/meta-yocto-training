DESCRIPTION = "Autotooled lib"
SECTION = "examples"
SUMMARY = "Autotooled lib"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = " virtual/gettext"

#SRC_URI = "file://${BPN}-${PV}.tar.gz"
SRCREV = "f3a9f332e3321ced76e4f9d6e68c2de8be01fd70"
#SRCREV = "${AUTOREV}"
#PV = "1.0.2+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libhw-autotooled-a.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit autotools

# By default EXTRA_OECONF is set to --disable-static ...
# Let's get rid of this
EXTRA_OECONF:remove = "--disable-static"
#DISABLE_STATIC = ""
#EXTRA_OECONF := "${@oe.utils.str_filter_out('--disable-static', '${EXTRA_OECONF}', d)}"
# enable static and disable shared (for fun)
EXTRA_OECONF += "--enable-static --disable-shared"

# allow empty (main) package
ALLOW_EMPTY:${PN} = "1"

# no main package here, statically linked not shared lib
#FILES:${PN}=""

# no main package, by default -dev depends on main
#RDEPENDS:${PN}-dev = ""

# made runtime dependency on -dev to we don't need to add both to image
# but -staticdev is sufficient
#RDEPENDS:${PN}-staticdev = "${PN}-dev (= ${EXTENDPKGV})"
