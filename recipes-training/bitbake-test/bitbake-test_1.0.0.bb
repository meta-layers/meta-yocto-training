DESCRIPTION = "bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
VAR_t1 = "value_t1"

python do_hard_immediate_assignment_py () {
  var1_t1 = d.getVar("VAR_t1", True)
  bb.debug(1, "Hard immediate Assignment")
  bb.debug(1, "VAR_t1 is %s\n" %var1_t1)
}

addtask do_hard_immediate_assignment_py
# <--

# -->
VAR_A_t2 ?= "1_t2"

VAR_B_t2 ?= "2_t2"
VAR_B_t2 ?= "3_t2"

VAR_C_t2 ?= "4_t2"
VAR_C_t2  = "5_t2"

python do_soft_immediate_assignment_py () {
  var1_t2 = d.getVar("VAR_A_t2", True)
  var2_t2 = d.getVar("VAR_B_t2", True)
  var3_t2 = d.getVar("VAR_C_t2", True)
  bb.debug(1, "Soft immediate Assignment")
  bb.debug(1, "VAR_A_t2 is %s\n" %var1_t2)
  bb.debug(1, "VAR_B_t2 is %s\n" %var2_t2)
  bb.debug(1, "VAR_C_t2 is %s\n" %var3_t2)
}

addtask do_soft_immediate_assignment_py
# <--

# -->
VAR_A_t3 ??= "1_t3"

VAR_B_t3 ??= "2_t3"
VAR_B_t3 ??= "3_t3"

VAR_C_t3  ?= "4_t3"
VAR_C_t3 ??= "5_t3"

VAR_D_t3   = "6_t3"
VAR_D_t3 ??= "7_t3"

python do_softer_lazy_assignment_py () {
  var1_t3 = d.getVar("VAR_A_t3", True)
  var2_t3 = d.getVar("VAR_B_t3", True)
  var3_t3 = d.getVar("VAR_C_t3", True)
  var4_t3 = d.getVar("VAR_D_t3", True)
  bb.debug(1, "Softer Lazy (delayed) Assignment")
  bb.debug(1, "VAR_A_t3 is %s\n" %var1_t3)
  bb.debug(1, "VAR_B_t3 is %s\n" %var2_t3)
  bb.debug(1, "VAR_C_t3 is %s\n" %var3_t3)
  bb.debug(1, "VAR_D_t3 is %s\n" %var4_t3)
}

addtask do_softer_lazy_assignment_py
# <--

# -->
VAR_A_t4  = "1_t4"
VAR_B_t4  = "${VAR_A_t4}"
VAR_A_t4  = "2_t4"

python do_lazy_variable_expansion_one_py () {
  var1_t4 = d.getVar("VAR_A_t4", True)
  var2_t4 = d.getVar("VAR_B_t4", True)
  bb.debug(1, "Lazy (delayed) variable expansion one")
  bb.debug(1, "VAR_A_t4 is %s\n" %var1_t4)
  bb.debug(1, "VAR_B_t4 is %s\n" %var2_t4)
}

addtask do_lazy_variable_expansion_one_py
# <--

# -->
VAR_A_t5 = "jumps over"
VAR_B_t5 = "The quick brown fox ${VAR_A_t5} the lazy dog."

python do_lazy_variable_expansion_two_py () {
  var1_t5 = d.getVar("VAR_A_t5", True)
  var2_t5 = d.getVar("VAR_B_t5", True)
  bb.debug(1, "Lazy (delayed) variable expansion two")
  bb.debug(1, "VAR_A_t5 is %s\n" %var1_t5)
  bb.debug(1, "VAR_B_t5 is %s\n" %var2_t5)
}

addtask do_lazy_variable_expansion_two_py
# <--

# -->
VAR_A_t6  = "1_t6"
VAR_B_t6 := "${VAR_A_t6}"
VAR_A_t6  = "2_t6"

python do_immediate_variable_expansion_one_py () {
  var1_t6 = d.getVar("VAR_A_t6", True)
  var2_t6 = d.getVar("VAR_B_t6", True)
  bb.debug(1, "Immediate variable expansion one")
  bb.debug(1, "VAR_A_t6 is %s\n" %var1_t6)
  bb.debug(1, "VAR_B_t6 is %s\n" %var2_t6)
}

addtask do_immediate_variable_expansion_one_py
# <--

# -->
VAR_A_t7  = "1_t7"
VAR_B_t7  = "VAR_B_t7:${VAR_A_t7}"
VAR_A_t7  = "2_t7"
VAR_C_t7  = "VAR_C_t7:${VAR_A_t7}"
VAR_D_t7 := "${VAR_B_t7} ${VAR_C_t7}"

python do_immediate_variable_expansion_two_py () {
  var1_t7 = d.getVar("VAR_A_t7", True)
  var2_t7 = d.getVar("VAR_B_t7", True)
  var3_t7 = d.getVar("VAR_C_t7", True)
  var4_t7 = d.getVar("VAR_D_t7", True)
  bb.debug(1, "Immediate variable expansion two")
  bb.debug(1, "VAR_A_t7 is %s\n" %var1_t7)
  bb.debug(1, "VAR_B_t7 is %s\n" %var2_t7)
  bb.debug(1, "VAR_C_t7 is %s\n" %var3_t7)
  bb.debug(1, "VAR_D_t7 is %s\n" %var4_t7)
}

addtask do_immediate_variable_expansion_two_py
# <--

# -->
VAR_A_t8  = "1_t8"
VAR_A_t8 .= "2_t8"

VAR_B_t8  = "3_t8"
VAR_B_t8 =. "4_t8"

python do_immediate_app_prep_no_spaces_one_py () {
  var1_t8 = d.getVar("VAR_A_t8", True)
  var2_t8 = d.getVar("VAR_B_t8", True)
  bb.debug(1, "Immediate append/prepend no spaces one")
  bb.debug(1, "VAR_A_t8 is %s\n" %var1_t8)
  bb.debug(1, "VAR_B_t8 is %s\n" %var2_t8)
}

addtask do_immediate_app_prep_no_spaces_one_py
# <--

# -->
VAR_A_t9        = "1_t9"
VAR_A_t9       .= "2_t9"
VAR_A_t9:append = "3_t9"

python do_immediate_app_prep_no_spaces_two_py () {
  var1_t9 = d.getVar("VAR_A_t9", True)
  bb.debug(1, "Immediate append/prepend no spaces vs. Lazy append/prepend no spaces override syntax one")
  bb.debug(1, "VAR_A_t9 is %s\n" %var1_t9)
}

addtask do_immediate_app_prep_no_spaces_two_py
# <--

# -->
VAR_A_t10        = "1_t10"
VAR_A_t10:append = "2_t10"
VAR_A_t10       .= "3_t10"

python do_lazy_app_prep_no_space_override_syntax_py () {
  var1_t10 = d.getVar("VAR_A_t10", True)
  bb.debug(1, "Immediate append/prepend no spaces vs. Lazy append/prepend no spaces override syntax two")
  bb.debug(1, "VAR_A_t10 is %s\n" %var1_t10)
}

addtask do_lazy_app_prep_no_space_override_syntax_py
# <--

# -->
VAR_A_t11        = "1_t11"
VAR_A_t11       += "2_t11"
VAR_A_t11:append = "3_t11"

python do_immediate_app_prep_with_spaces_vs_no_space_override_syntax_one_py () {
  var1_t11 = d.getVar("VAR_A_t11", True)
  bb.debug(1, "Immediate append/prepend with spaces vs. Lazy append/prepend no spaces override syntax one")
  bb.debug(1, "VAR_A_t11 is %s\n" %var1_t11)
}

addtask do_immediate_app_prep_with_spaces_vs_no_space_override_syntax_one_py
# <--

# -->
VAR_A_t12        = "1_t12"
VAR_A_t12:append = "2_t12"
VAR_A_t12       += "3_t12"

python do_immediate_app_prep_with_spaces_vs_no_space_override_syntax_two_py () {
  var1_t12 = d.getVar("VAR_A_t12", True)
  bb.debug(1, "Immediate append/prepend with spaces vs. Lazy append/prepend no spaces override syntax two")
  bb.debug(1, "VAR_A_t12 is %s\n" %var1_t12)
}

addtask do_immediate_app_prep_with_spaces_vs_no_space_override_syntax_two_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
OVERRIDES:append = ":left:middle:right"
VAR_A_t13 = "1_t13"
VAR_A_t13:left = "2_t13"

python do_conditional_var_one_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t13 = d.getVar("VAR_A_t13", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t13 is %s\n" %var1_t13)
}

addtask do_conditional_var_one_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
#OVERRIDES:append = ":left:middle:right"
VAR_A_t14 = "1_t14"
VAR_A_t14:left = "2_t14"
VAR_A_t14:right = "3_t14"

python do_conditional_var_two_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t14 = d.getVar("VAR_A_t14", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t14 is %s\n" %var1_t14)
}

addtask do_conditional_var_two_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
#OVERRIDES:append = ":left:middle:right"
VAR_A_t15 = "1_t15"
VAR_A_t15:left:append = "2_t15"

python do_conditional_var_three_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t15 = d.getVar("VAR_A_t15", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t15 is %s\n" %var1_t15)
}

addtask do_conditional_var_three_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
#OVERRIDES:append = ":left:middle:right"
VAR_A_t16 = "1_t16"
VAR_A_t16:append:left = "2_t16"

python do_conditional_var_four_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t16 = d.getVar("VAR_A_t16", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t16 is %s\n" %var1_t16)
}

addtask do_conditional_var_four_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
#OVERRIDES:append = ":left:middle:right"
VAR_A_t17 = "1_t17"
VAR_A_t17:left:append = "2_t17"
# append with space:
VAR_A_t17:left:append += "3_t17"
# append without space:
# VAR_A_t17_left:append = "3_t17"

python do_conditional_var_five_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t17 = d.getVar("VAR_A_t17", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t17 is %s\n" %var1_t17)
}

addtask do_conditional_var_five_py
# <--

# -->
#OVERRIDES:prepend = "left:middle:right:"
#OVERRIDES:append = ":left:middle:right"
VAR_A_t18 = "1_t18"
VAR_A_t18:append = "2_t18"
VAR_A_t18:append = "3_t18"
VAR_A_t18 += "4_t18"
VAR_A_t18 .= "5_t18"

python do_conditional_var_six_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t18 = d.getVar("VAR_A_t18", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t18 is %s\n" %var1_t18)
}

addtask do_conditional_var_six_py
# <--

# -->
VAR_A_t19 = "123 456 789 123456 123 456 123 456"
VAR_A_t19:remove = "123"
VAR_A_t19:remove = "456"

python do_removal_one_py () {
  var1_t19 = d.getVar("VAR_A_t19", True)
  bb.debug(1, "Removal (Override Style Syntax)")
  bb.debug(1, "VAR_A_t19 is %s\n" %var1_t19)
}

addtask do_removal_one_py
# <--

# -->
VAR_A_t20 = "1_t20"
VAR_A_t20[doc] = "This is a test variable flag"
VAR_A_t20[a] = "abc"
VAR_A_t20[b] = "123"
VAR_A_t20[a] += "456"

python do_varflags_one_py () {
  vf  = d.getVarFlags("PACKAGECONFIG") or {}
  vf1 = d.getVarFlags("VAR_A_t20")
  bb.debug(1, "Varflags one")
  bb.debug(1, "variable flags PACKAGECONFIG is %s\n" %vf)
  bb.debug(1, "variable flags VAR_A_t20 is %s\n" %vf1)
  
}

addtask do_varflags_one_py
# <--

