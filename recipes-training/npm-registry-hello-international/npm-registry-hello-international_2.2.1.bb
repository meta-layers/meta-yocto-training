SUMMARY = "Common Greetings in 10+ Languages "
HOMEPAGE = "https://github.com/vidaaudrey/hello-international#readme"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${UNPACKDIR}/node_modules/lodash/LICENSE;md5=f519f8cb155e30cece6b0e75aa6a3a53"

SRC_URI = " \
    npm://registry.npmjs.org/;package=hello-international;version=${PV} \
    npmsw://${THISDIR}/${BPN}/npm-shrinkwrap.json \
    "

S = "${WORKDIR}/npm"

inherit npm

LICENSE:${PN}-lodash = "MIT"
LICENSE:${PN} = "MIT"

# --> added test case

SRC_URI += " \
    file://test.js \
    "

do_install:prepend() {
    # for some reason we need to explicity also copy lodash (new npm class?)
    cp -r ${UNPACKDIR}/node_modules/lodash ${NPM_BUILD}/lib/node_modules/
    # testcase
    cp ${UNPACKDIR}/test.js ${NPM_BUILD}/lib/node_modules/hello-international/
}

# <-- added test case

# Note: ???
# You need to define e.g. in local.conf:
# WWWDIR = "${localstatedir}/www/localhost"

# initial_rev .: f64939003921cbaf57e21393d654fdf49d61918f
#python do_configure:append() {
#    pkgdir = d.getVar("NPM_PACKAGE")
#    lockfile = os.path.join(pkgdir, "singletask.lock")
#    bb.utils.remove(lockfile)
#}

