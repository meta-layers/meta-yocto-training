DESCRIPTION = "Use autotooled lib"
SECTION = "examples"
SUMMARY = "Use autotooled lib"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "f0933bfbb8c58024312a4219c91255654086791e"
#SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/exempli-gratia/use-autotooled-lib.git;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
# https://stackoverflow.com/questions/55542738/yocto-ci-build-number-pr-service-do-not-increment-pr
# https://docs.windriver.com/bundle/Wind_River_Linux_Toolchain_and_Build_System_Users_Guide_LTS_19/page/mmo1403548674799.html
#PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

DEPENDS = "libhw"
#RDEPENDS:${PN} = "libhw"

do_compile() {
        ${CC} -Werror use-autotooled-lib.c ${CFLAGS} ${LDFLAGS} -l hw -o use-autotooled-lib
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 use-autotooled-lib ${D}${bindir}
}
