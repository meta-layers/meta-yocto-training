DESCRIPTION = "elce 2021 bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
GUESTS_t01 = "jasmine"
GUESTS_t01 += "christian"

python do_plus_eq_py () {
  var1_GUESTS_t01 = d.getVar("GUESTS_t01", True)
  bb.warn("GUESTS_t01 is %s\n" %var1_GUESTS_t01)
}

addtask do_plus_eq_py
# GUESTS_t01 is jasmine christian
# <--

# -->
GUESTS_t02 = "aaron"
GUESTS_t02 =+ "marie"

python do_eq_plus_py () {
  var1_GUESTS_t02 = d.getVar("GUESTS_t02", True)
  bb.warn("GUESTS_t02 is %s\n" %var1_GUESTS_t02)
}

addtask do_eq_plus_py
# GUESTS_t02 is marie aaron
# <--

# -->
GUESTS_t03 += "muhammad"
# has nothing to do with above, 
# we just overwrite the variable here
GUESTS_t03 = "ilana"

python do_plus_eq_first_py () {
  var1_GUESTS_t03 = d.getVar("GUESTS_t03", True)
  bb.warn("GUESTS_t03 is %s\n" %var1_GUESTS_t03)
}

addtask do_plus_eq_first_py
# GUESTS_t03 is ilana
# <--

# -->
MARK_t04 = "B"
MARK_t04 .= "+"

python do_dot_eq_py () {
  var1_MARK_t04 = d.getVar("MARK_t04", True)
  bb.warn("MARK_t04 is %s\n" %var1_MARK_t04)
}

addtask do_dot_eq_py
# MARK_t04 is B+
# <--

# -->
MONEY_t05 = "524,00$"
MONEY_t05 =. "1000000"

python do_eq_dot_py () {
  var1_MONEY_t05 = d.getVar("MONEY_t05", True)
  bb.warn("MONEY_t05 is %s\n" %var1_MONEY_t05)
}

addtask do_eq_dot_py
# MONEY_t05 is 1000000524,00$
# <--

# -->
MARK_t06 .= "+"
MARK_t06 = "B"

python do_dot_eq_first_py () {
  var1_MARK_t06 = d.getVar("MARK_t06", True)
  bb.warn("MARK_t06 is %s\n" %var1_MARK_t06)
}

addtask do_dot_eq_first_py
# MARK_t06 is B
# <--

# -->
BUFFET_t07 ?= "tacos croissant baklava sushi"
BUFFET_t07 += "curry"

python do_quest_eq_py () {
  var1_BUFFET_t07 = d.getVar("BUFFET_t07", True)
  bb.warn("BUFFET_t07 is %s\n" %var1_BUFFET_t07)
}

addtask do_quest_eq_py
# BUFFET_t07 is tacos croissant baklava sushi curry
# <--

# -->
BUFFET_t08 ??= "lokum gyoza hummus"
BUFFET_t08 ?= "pizza"
BUFFET_t08 += "paella"

python do_quest_quest_eq_py () {
  var1_BUFFET_t08 = d.getVar("BUFFET_t08", True)
  bb.warn("BUFFET_t08 is %s\n" %var1_BUFFET_t08)
}

addtask do_quest_quest_eq_py
# BUFFET_t08 is pizza paella
# <--

# --> error in presentation
BUFFET_t09 += "ice-cream"
BUFFET_t09 ?= "schnitzel blini"

python do_plus_eq_quest_eq_py () {
  var1_BUFFET_t09 = d.getVar("BUFFET_t09", True)
  bb.warn("BUFFET_t09 is %s\n" %var1_BUFFET_t09)
}

addtask do_plus_eq_quest_eq_py
# BUFFET_t09 is  ice-cream
# additional space?

# + bitbake bitbake-test-elce-2021 -e
# + awk '/^# \$BUFFET_t09 \[/,/^BUFFET_t09/'
# $BUFFET_t09 [2 operations]
#   append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021_1.0.0.bb:114
#     "ice-cream"
#   set? /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021_1.0.0.bb:115
#     "schnitzel blini"
# pre-expansion value:
#   " ice-cream"
# BUFFET_t09=" ice-cream"

# <-- error in presentation
