DESCRIPTION = ".a, .so library made with cmake"
SECTION = "examples"
SUMMARY = ".a, .so library made with cmake"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "3c0a6ff6f90693d8a5a30fa21dc2ec12dbd1388c"
#SRCREV = "${AUTOREV}"
#PV = "1.0.3+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libhwtest-cmake-so-a.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=ON -DLHWC_WITH_SHARED=ON"

# we need the empty main pkg, as -staticdev depends on it => they end up in the SDK
# ALLOW_EMPTY:${PN} = "1"
