DESCRIPTION = "Npm package to append "This is a sample message" message to "Hello World" text"
SECTION = "examples"
SUMMARY = "Npm package to append "This is a sample message" message to "Hello World" text"
HOMEPAGE = "https://github.com/RobertBerger/js-npm-hello-world"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d461797431fe3ed1905a92ea25694c63"

SRC_URI = " \
    git://github.com/RobertBerger/js-npm-hello-world;protocol=https;branch=master \
    "

# Modify these as desired
#SRCREV = "${AUTOREV}"
SRCREV = "e2779a213bbe99cd789bf1ef4e9f2e3d79c341ad"
#PV = "1.0.1+git${SRCPV}"

S = "${WORKDIR}/git"

inherit npm

LICENSE:${PN} = "MIT"

# Note: ?????? no ????
# You need to define e.g. in local.conf:
# WWWDIR = "${localstatedir}/www/localhost"

# nodejs depends on brotli, but brotli is not installed ???
# same with c-are and icu
RDEPENDS:${PN} += "brotli"
RDEPENDS:${PN} += "c-ares"
RDEPENDS:${PN} += "icu"
RDEPENDS:${PN} += "nodejs"
