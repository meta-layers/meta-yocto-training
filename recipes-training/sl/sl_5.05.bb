DESCRIPTION = "sl (Steam Locomotive): Cure your bad habit of mistyping"
SECTION = "examples"
SUMMARY = "sl (Steam Locomotive)"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=74e9dd589a0ab212a9002b15ef2b82f0"

#FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${PV}:"

TAG:="${PV}"

SRCREV ?= "085764117c72ed7d9387fb09a27d96003d36090f"
#SRCREV = "${AUTOREV}"
# don't use tag, since like this an offline build won't work
# https://wiki.yoctoproject.org/wiki/How_do_I#Q:_How_do_I_create_my_own_source_download_mirror_?
#SRC_URI = "git://gitlab.com/exempli-gratia/${BPN}.git;protocol=https;branch=master;tag=${TAG}"
SRC_URI = "git://gitlab.com/exempli-gratia/${BPN}.git;protocol=https;branch=master"
SRC_URI += "file://0001-don-t-hardcode-the-tools-and-flags.patch"
SRC_URI += "file://0002-QA-Issue-File-usr-bin-sl-in-package-sl-doesn-t-have-.patch"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
#PV = "${TAG}+git${SRCPV}"

# :append .=, += would work as well
DEPENDS:append = " ncurses"
# This works as well:
#DEPENDS = "ncurses"
# Here we don't include ncurses"
#DEPENDS ?= " ncurses"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

do_install() {
        install -d ${D}${bindir}
        install -m 0755 sl ${D}${bindir}
}
