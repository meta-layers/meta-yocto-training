DESCRIPTION = "bitbake trace var change"
SECTION = "examples"
SUMMARY = "bitbake trace var change"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
VAR_t1:remove = " (_append)val3_t1"
VAR_t1 =  "val1_t1"
VAR_t1 += "(+=)val2_t1"
VAR_t1:append = " (_append)val3_t1"
VAR_t1 += "(+=)val2_t1"
#VAR_t1:remove = " val3_t1"
VAR_t1:append = " (_append)val4_t1"

python do_trace_var_py () {
  var1_t1 = d.getVar("VAR_t1", True)
  bb.warn("VAR_t1 is %s\n" %var1_t1)
}

addtask do_trace_var_py
# <--
