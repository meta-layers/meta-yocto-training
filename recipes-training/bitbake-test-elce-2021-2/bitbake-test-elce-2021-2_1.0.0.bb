DESCRIPTION = "elce 2021 bitbake test 2"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# --> error in presentation
# cinnamon-buns recipe
INGREDIENTSt10 ?= "cinnamon flour butter eggs milk sugar"

python do_quest_eq_py () {
  var1_INGREDIENTSt10 = d.getVar("INGREDIENTSt10", True)
  bb.warn("INGREDIENTSt10 is %s\n" %var1_INGREDIENTSt10)
}

addtask do_quest_eq_py
# without modified local.conf:
# result:
#    INGREDIENTSt10 is "cinnamon flour butter eggs milk sugar"
#
# with modified local.conf:
# 1)
#    INGREDIENTSt10 += "glaze vanilla-extract"
#    Note:
#        += -> local.conf is executed before recipe
#              so we overwrite INGREDIENTSt10
# result:
#    INGREDIENTSt10 is " glaze vanilla-extract"
#
# + bitbake bitbake-test-elce-2021-2 -e
# + grep '^INGREDIENTSt10='
# INGREDIENTSt10=" glaze vanilla-extract"
# + bitbake bitbake-test-elce-2021-2 -e
# + awk '/^# \$INGREDIENTSt10 \[/,/^INGREDIENTSt10/'
#  $INGREDIENTSt10 [2 operations]
#    append /workdir/build/multi-v7-ml-debug-training/conf/local.conf:278
#      "glaze vanilla-extract"
#    set? /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-2_1.0.0.bb:8
#      "cinnamon flour butter eggs milk sugar"
#  pre-expansion value:
#    " glaze vanilla-extract"
# INGREDIENTSt10=" glaze vanilla-extract"
# <-- error in presentation

# --> 
# without (another) modification in local.conf:
# result:
#    INGREDIENTSt10 is "cinnamon flour butter eggs milk sugar"
#
# with modified local.conf:
# 2)
#    INGREDIENTSt10:append = " glaze vanilla-extract"
#    Note:
#        _append = --> _append is without space, we need to add one
# result:
#    INGREDIENTSt10 is "cinnamon flour butter eggs milk sugar glaze vanilla-extract"
#
# + bitbake bitbake-test-elce-2021-2 -e
# + grep '^INGREDIENTSt10='
# INGREDIENTSt10="cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# + bitbake bitbake-test-elce-2021-2 -e
# + awk '/^# \$INGREDIENTSt10 \[/,/^INGREDIENTSt10/'
#  $INGREDIENTSt10 [2 operations]
#    _append /workdir/build/multi-v7-ml-debug-training/conf/local.conf:280
#      " glaze vanilla-extract"
#    set? /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-2_1.0.0.bb:8
#      "cinnamon flour butter eggs milk sugar"
#  pre-expansion value:
#    "cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# INGREDIENTSt10="cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# <--

# --> 
# without (another) modification in local.conf:
# result:
#    INGREDIENTSt10 is "cinnamon flour butter eggs milk sugar"
#
# with modified local.conf:
# 3)
#    INGREDIENTSt10:append += "glaze vanilla-extract"
#    Note: 
#         _append += --> _append + leading space
# result:
#    INGREDIENTSt10 is "cinnamon flour butter eggs milk sugar glaze vanilla-extract"
#
# + bitbake bitbake-test-elce-2021-2 -e
# + grep '^INGREDIENTSt10='
# INGREDIENTSt10="cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# + bitbake bitbake-test-elce-2021-2 -e
# + awk '/^# \$INGREDIENTSt10 \[/,/^INGREDIENTSt10/'
#  $INGREDIENTSt10 [2 operations]
#    _append /workdir/build/multi-v7-ml-debug-training/conf/local.conf:281
#      " glaze vanilla-extract"
#    set? /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-2_1.0.0.bb:8
#      "cinnamon flour butter eggs milk sugar"
#  pre-expansion value:
#    "cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# INGREDIENTSt10="cinnamon flour butter eggs milk sugar glaze vanilla-extract"
# <--
