DESCRIPTION = "Hello World by Cargo for Rust"
SECTION = "examples"
SUMMARY = "Hello World by Cargo for Rust"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com, https://github.com/meta-rust/rust-hello-world"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT | Apache-2.0"
LIC_FILES_CHKSUM="file://COPYRIGHT;md5=e6b2207ac3740d2d01141c49208c2147"

inherit cargo

SRC_URI = "git://gitlab.com/exempli-gratia/rust-hello-world.git;protocol=https;branch=main"
SRCREV="62537fcd93ef5fae6b23a1931342e6a0e380a398"
#SRCREV = "${AUTOREV}"
#PV = "0.0+git${SRCPV}"

S = "${WORKDIR}/git"

BBCLASSEXTEND = "native"

# --->
# [ 2024-01-02 16:32:37] NOTE: recipe rust-hello-world-0.0+gitAUTOINC+62537fcd93-r0: task do_compile: Started
# ERROR: rust-hello-world-0.0+gitAUTOINC+62537fcd93-r0 do_compile: ExecutionError('/workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello- orld/0.0+gitAUTOINC+62537fcd93/temp/run.do_compile.1242', 101, None, None)
# ERROR: Logfile of failure stored in: /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/temp/log.do_ ompile.1242
# [2024-01-02 16:32:37] Log data follows:
# [2024-01-02 16:32:37] | DEBUG: Executing shell function do_compile
# ERROR: Task (/workdir/sources/poky-training-master/meta/recipes-extended/95.21_rust-hello-world/rust-hello-world_git.bb:do_compile) failed with exit code '1'
# [2024-01-02 16:32:37] | NOTE: Using rust targets from /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537f d93/rust-targets/
# [2024-01-02 16:32:37] | NOTE: cargo = /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/recipe-sysr ot-native/usr/bin/cargo
# [2024-01-02 16:32:37] | NOTE: cargo build -v --frozen --target armv7-resy-linux-gnueabihf --release --manifest-path=/workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf- eon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/git//Cargo.toml
# [2024-01-02 16:32:37] | error: the lock file /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/git/ argo.lock needs to be updated but --frozen was passed to prevent this
# [2024-01-02 16:32:37] | If you want to try to generate the lock file without accessing the network, remove the --frozen flag and use --offline instead.
# [2024-01-02 16:32:37] | WARNING: /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/temp/run.do_compile.1242:313 exit 101 from '"cargo" build -v --frozen --target armv7-resy-linux-gnueabihf --release --manifest-path=/workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf- eon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/git//Cargo.toml "$@"'
# [2024-01-02 16:32:37] | WARNING: Backtrace (BB generated script):
# [2024-01-02 16:32:37] |         #1: oe_cargo_build, /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd 3/temp/run.do_compile.1242, line 313
# [2024-01-02 16:32:37] |         #2: cargo_do_compile, /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537f d93/temp/run.do_compile.1242, line 162
# [2024-01-02 16:32:37] |         #3: do_compile, /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/t mp/run.do_compile.1242, line 157
# [2024-01-02 16:32:37] |         #4: main, /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/rust-hello-world/0.0+gitAUTOINC+62537fcd93/temp/ru .do_compile.1242, line 333
# [2024-01-02 16:32:37] NOTE: recipe rust-hello-world-0.0+gitAUTOINC+62537fcd93-r0: task do_compile: Failed
# [2024-01-02 16:32:38] NOTE: Tasks Summary: Attempted 958 tasks of which 946 didn't need to be rerun and 1 failed.
# [2024-01-02 16:32:38] NOTE: The errors for this build are stored in /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/log/error-report/error_report_20240102163230.txt
# [2024-01-02 16:32:38] You can send the errors to a reports server by running:
# [2024-01-02 16:32:38]   send-error-report /workdir/build/multi-v7-ml-debug-training-pkgs-master/tmp/log/error-report/error_report_20240102163230.txt [-s server]
# [2024-01-02 16:32:38] NOTE: The contents of these logs will be posted in public if you use the above command with the default server. Please ensure you remove any identifying or proprietary  nformation when prompted before sending.
# [2024-01-02 16:32:38] NOTE: Writing buildhistory
# [2024-01-02 16:32:40] NOTE: Writing buildhistory took: 2 seconds
# [2024-01-02 16:32:40] 
# [2024-01-02 16:32:40] Summary: 1 task failed:
# [2024-01-02 16:32:40]   /workdir/sources/poky-training-master/meta/recipes-extended/95.21_rust-hello-world/rust-hello-world_git.bb:do_compile
# [2024-01-02 16:32:40] Summary: There was 1 ERROR message, returning a non-zero exit code.
# <---

# Wwhat seems to fix it as indicated in the log above
# "If you want to try to generate the lock file without accessing the network, remove the --frozen flag and use --offline instead."
# And that's what we do here:
CARGO_BUILD_FLAGS:remove = "--frozen"
CARGO_BUILD_FLAGS:append = " --offline"
