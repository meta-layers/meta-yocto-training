DESCRIPTION = "bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
#OVERRIDES:prepend = "sun:rain:snow:"
OVERRIDES:append = ":sun:rain:snow"
VAR_PROTECTION_t40     = "sun"
VAR_PROTECTION_t40:sun = "lotion"

python do_p76_1_py () {
  overrides = d.getVar("OVERRIDES", True)
  var_protection_t40 = d.getVar("VAR_PROTECTION_t40", True)
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_PROTECTION_t40 is %s\n" %var_protection_t40)
}

addtask do_p76_1_py
# <--

# -->
#OVERRIDES:prepend = "sun:rain:snow:"
#OVERRIDES:append = ":sun:rain:snow"
VAR_PROTECTION_t41:rain = "umbrella"
VAR_PROTECTION_t41:snow = "sweater"

python do_p76_2_py () {
  overrides = d.getVar("OVERRIDES", True)
  var_protection_t41 = d.getVar("VAR_PROTECTION_t41", True)
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_PROTECTION_t41 is %s\n" %var_protection_t41)
}

addtask do_p76_2_py
# <--

# -->
#OVERRIDES:prepend = "sun:rain:snow:"
#OVERRIDES:append = ":sun:rain:snow"
VAR_PROTECTION_t42:rain = "umbrella"
VAR_PROTECTION_t42:hail = "duck"

python do_p76_3_py () {
  overrides = d.getVar("OVERRIDES", True)
  var_protection_t42 = d.getVar("VAR_PROTECTION_t42", True)
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_PROTECTION_t42 is %s\n" %var_protection_t42)
}

addtask do_p76_3_py
# <--


# -->

# moved to another testcase

#OVERRIDES:prepend = "sun:rain:snow:"
#OVERRIDES:append = ":sun:rain:snow"
#OVERRIDES:append = ":${OTHER}"
#OTHER = "hail"
#VAR_PROTECTION_t43_rain = "umbrella"
#VAR_PROTECTION_t43_hail = "duck"
#
#python do_p76_4_py () {
#  overrides = d.getVar("OVERRIDES", True)
#  var_protection_t43 = d.getVar("VAR_PROTECTION_t43", True)
#  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
#  bb.debug(1, "VAR_PROTECTION_t43 is %s\n" %var_protection_t43)
#}

#addtask do_p76_4_py
# <--

# -->
#OVERRIDES:prepend = "sun:rain:snow:"
#OVERRIDES:append = ":sun:rain:snow"
VAR_PROTECTION_t44             = "sweater"
VAR_PROTECTION_t44:append:rain = "umbrella"

python do_p76_5_py () {
  overrides = d.getVar("OVERRIDES", True)
  var_protection_t44 = d.getVar("VAR_PROTECTION_t44", True)
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_PROTECTION_t44 is %s\n" %var_protection_t44)
}

addtask do_p76_5_py
# <--



