DESCRIPTION = "bitbake test 1"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
VAR_t1 ?= "value_t1"

do_benedikt_py:prepend () {
  bb.debug(1, "I am from prepend 1")
}

do_benedikt_py:prepend () {
  bb.debug(1, "I am from prepend 2")
}

python do_benedikt_py () {
  var1_t1 = d.getVar("VAR_t1", True)
  bb.debug(1, "Hard immediate Assignment")
  bb.debug(1, "VAR_t1 is %s\n" %var1_t1)
}

addtask do_benedikt_py
# <--



