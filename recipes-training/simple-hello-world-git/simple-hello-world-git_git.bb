DESCRIPTION = "Simple helloworld application + git"
SECTION = "examples"
SUMMARY = "Simple helloworld application + git"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "ab7730a2f39660b573fae9e9c20e8c33f8a642db"
#SRCREV = "${AUTOREV}"
#SRC_URI = "git://gitlab.com/exempli-gratia/simple-hello-world-git.git;protocol=https;branch=master"
SRC_URI = "git:///workdir/sources/meta-yocto-training-sources/simple-hello-world-git;protocol=file;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
# https://stackoverflow.com/questions/55542738/yocto-ci-build-number-pr-service-do-not-increment-pr
# https://docs.windriver.com/bundle/Wind_River_Linux_Toolchain_and_Build_System_Users_Guide_LTS_19/page/mmo1403548674799.html
PV = "1.0.1+git${SRCPV}"

S = "${WORKDIR}/git"

do_compile() {
        ${CC} -Werror simple-hello-world-git.c ${CFLAGS} ${LDFLAGS} -o simple-hello-world-git
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 simple-hello-world-git ${D}${bindir}
}

# Note: added ${LDFLAGS} in morty to get rid of:
#       do_package_qa: QA Issue: No GNU_HASH in the elf binary

#DEFAULT_PREFERENCE = "-1"
