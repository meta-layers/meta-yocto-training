DESCRIPTION = ".a library made with cmake"
SECTION = "examples"
SUMMARY = ".a library made with cmake"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "b22423e39deda852d4193dbe2ca1702e3eec0291"
#SRCREV = "${AUTOREV}"
#PV = "1.0.3+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libhw-cmake-a.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=ON -DLHWC_WITH_SHARED=OFF"
