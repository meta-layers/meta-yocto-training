DESCRIPTION = "bitbake immediate/lazy"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
DATE1t18 = "${@('(1)', int(round(time.time() * 1000)))}"
DATE2t18 = "${DATE1t18}"
DATE1t18 = "${@('(2)', int(round(time.time() * 1000)))}"

DATE3t18 = "${@('(3)', int(round(time.time() * 1000)))}"
DATE4t18 = "${DATE3t18}"
DATE3t18 = "${@('(4)', int(round(time.time() * 1000)))}"

# DATE2t18="('(2)', 1622444548146)"
# DATE1t18="('(2)', 1622444548146)"
# DATE4t18="('(4)', 1622444548147)"
# DATE3t18="('(4)', 1622444548147)"
# <--

# -->
DATE1t19 = "${@('(11)', int(round(time.time() * 1000)))}"
DATE2t19 := "${DATE1t19}"
DATE1t19 = "${@('(12)', int(round(time.time() * 1000)))}"

DATE3t19 = "${@('(13)', int(round(time.time() * 1000)))}"
DATE4t19 := "${DATE3t19}"
DATE3t19 = "${@('(14)', int(round(time.time() * 1000)))}"

# DATE2t19="('(11)', 1622444556948)"
# DATE1t19="('(12)', 1622444557083)"
# DATE4t19="('(13)', 1622444556949)"
# DATE3t19="('(14)', 1622444557084)"
# <--
