DESCRIPTION = "boost filesystem with cmake"
SECTION = "examples"
SUMMARY = "boost filesystem with cmake"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "42961f416a3f71573c0785a8ffb34f4de1c20f36"
#SRCREV = "${AUTOREV}"
#PV = "1.0.3+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/boost-filesystem-cmake.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

DEPENDS = "boost"

inherit cmake

EXTRA_OECMAKE = ""

# $ cat buildhistory/images/multi_v7_ml/glibc/core-image-minimal/installed-package-names.txt | fgrep libboost
# libboost-filesystem1.72.0
# libboost-locale1.72.0
# libboost-thread1.72.0

# $ cat buildhistory/images/multi_v7_ml/glibc/core-image-minimal/depends-nokernel.dot | grep libboost
# "boost-filesystem-cmake" -> "libboost-filesystem1.72.0"
# "boost-filesystem-cmake" -> "libboost-locale1.72.0"
# "libboost-filesystem1.72.0" -> "libc6"
# "libboost-filesystem1.72.0" -> "libgcc1"
# "libboost-filesystem1.72.0" -> "libstdc++6"
# "libboost-locale1.72.0" -> "libboost-thread1.72.0"
# "libboost-locale1.72.0" -> "libc6"
# "libboost-locale1.72.0" -> "libgcc1"
# "libboost-locale1.72.0" -> "libicui18n66"
# "libboost-locale1.72.0" -> "libicuuc66"
# "libboost-locale1.72.0" -> "libstdc++6"
# "libboost-thread1.72.0" -> "libc6"
# "libboost-thread1.72.0" -> "libgcc1"
# "libboost-thread1.72.0" -> "libstdc++6"
