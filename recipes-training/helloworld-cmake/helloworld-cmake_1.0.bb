DESCRIPTION = "Simple helloworld cmake application"
SECTION = "examples"
SUMMARY = "Simple helloworld cmake application"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

#SRCREV = "9301cda88f2acf98439d459ee0776c462d806358"
SRCREV = "${AUTOREV}"
PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/helloworld-cmake.git;protocol=https;branch=master"
S = "${WORKDIR}/git"
#UNPACKDIR = "${S}"

inherit cmake

EXTRA_OECMAKE = ""


# devtool unmodified:
#LICENSE = "MIT"
#LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"
#
#SRC_URI = "git://gitlab.com/exempli-gratia/helloworld-cmake.git;protocol=https;branch=master"
#
## Modify these as desired
#PV = "1.0+git"
#SRCREV = "9301cda88f2acf98439d459ee0776c462d806358"
#
#S = "${WORKDIR}/git"
#
#inherit cmake
#
## Specify any options you want to pass to cmake using EXTRA_OECMAKE:
#EXTRA_OECMAKE = ""

