DESCRIPTION = "Simple helloworld cpp cmake application"
SECTION = "examples"
SUMMARY = "Simple helloworld cpp cmake application"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "fa5b0b67d1668655a9b826b4f2152376d4b9c9d2"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/hellocppcmake.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE = ""
