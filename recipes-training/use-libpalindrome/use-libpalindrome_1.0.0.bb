DESCRIPTION = "Cpp/cmake exe which uses a lib which finds if word is a palindrome"
SECTION = "examples"
SUMMARY = "Cpp/cmake exe which uses a lib which finds if word is a palindrome"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "6b0c3169df1e278f296bb79f9d7df5d2fd442a50"
#SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/exempli-gratia/use-libpalindrome-cpp.git;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# simple-hello-world-git-dev_0.0+git4+ecf1f0bc87-r0.0_armv7a-vfp-neon.ipk
# https://stackoverflow.com/questions/55542738/yocto-ci-build-number-pr-service-do-not-increment-pr
# https://docs.windriver.com/bundle/Wind_River_Linux_Toolchain_and_Build_System_Users_Guide_LTS_19/page/mmo1403548674799.html
#PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit cmake

DEPENDS = "libpalindrome-cpp-cmake-so-a"

# if only .a  exists  -> staically linked
# if only .so exists  -> dynamically linked
# if .a and .so exist -> dynamically linked
