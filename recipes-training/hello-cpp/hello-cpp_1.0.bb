DESCRIPTION = "Simple helloworld application"
SECTION = "examples"
SUMMARY = "Simple helloworld application"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "5669bcd6894a27e95454aadcea57054dd11b7612"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/hello-cpp.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

do_compile() {
         ${CXX} hello-cpp.cpp ${CFLAGS} ${LDFLAGS} -o hello-cpp
}

do_install() {
         install -d ${D}${bindir}
         install -m 0755 hello-cpp ${D}${bindir}
}
