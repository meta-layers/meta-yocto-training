DESCRIPTION = ".so library made with cmake"
SECTION = "examples"
SUMMARY = ".so library made with cmake"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "f733ec9f32e6d970381ccacc364da3dee0e8a0fd"
#SRCREV = "${AUTOREV}"
#PV = "1.0.3+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/libhw-cmake-so.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=OFF -DLHWC_WITH_SHARED=ON"
