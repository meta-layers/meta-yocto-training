DESCRIPTION = "Simple helloworld application"
SECTION = "examples"
SUMMARY = "Simple helloworld application"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1d10f40055ab4fb8f37eb56febff1d19"

SRCREV = "662b28a75f4a904132cf45ebab0b3634e43fa4a5"
#SRCREV = "${AUTOREV}"
#PV = "1.0.0+git${SRCPV}"
# upstream:
SRC_URI = "git://gitlab.com/exempli-gratia/simple-hello-world.git;protocol=https;branch=master"
S = "${WORKDIR}/git"

do_compile() {
#       bbwarn "--> S: ${S} <--"
        ${CC} -Werror simple-hello-world.c ${CFLAGS} ${LDFLAGS} -o simple-hello-world
#       ${CC} -Werror simple-hello-world.c ${LDFLAGS} -o simple-hello-world
#        will fail:
#        ${CC} -Werror simple-hello-world.c ${CFLAGS} -o simple-hello-world
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 simple-hello-world ${D}${bindir}
}

# this is a python task:
python do_printdate () {
    import datetime
    # goes to cooker shell:
    bb.warn('py: --> S: %s <--' % (d.getVar("S", True)))
    bb.plain('py: Date: %s' % (datetime.date.today()))
    # goes to log:
    print(datetime.datetime.now())
    print(datetime.date.today())
    # goes to cooker shell:
    bb.plain("py: Check the logs!")
    bb.plain('py: this log file: %s/log.do_printdate' % (d.getVar("T", True)))
}
addtask do_printdate after do_compile before do_install

# this is a shell task:
do_s () {
    bbwarn "shell: --> S: ${S} <--"
}
addtask do_s before do_printdate

# Note: added ${LDFLAGS} in morty to get rid of:
#       do_package_qa: QA Issue: No GNU_HASH in the elf binary
