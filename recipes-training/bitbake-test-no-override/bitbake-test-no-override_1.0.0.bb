DESCRIPTION = "bitbake test no override"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
VAR_A_t15 = "1_t15"
VAR_A_t15_left:append = "2_t15"

python do_conditional_var_no_overrides_one_py () {
  overrides = d.getVar("OVERRIDES", True)
  var1_t15 = d.getVar("VAR_A_t15", True)
  var2_t15 = d.getVar("VAR_A_t15_left", True)
  bb.debug(1, "Conditional variable one")
  bb.debug(1, "OVERRIDES (now) is %s\n" %overrides)
  bb.debug(1, "VAR_A_t15 is %s\n" %var1_t15)
  bb.debug(1, "VAR_A_t15_left is %s\n" %var2_t15)
}

addtask do_conditional_var_no_overrides_one_py
# <--

# -->
#VAR_A_t21${VAR_B_t21} = "1_t21"
#VAR_B_t21 = "2_t21"
#VAR_A_t212_t21 = "3_t21"
#
#python do_key_expansion_one_py () {
#  var2_t21 = d.getVar("VAR_B_t21", True)
#  var3_t21 = d.getVar("VAR_A_t212_t21", True)
#  bb.debug(1, "Key expansion one")
#  bb.debug(1, "VAR_B_t21 is %s\n" %var2_t21)
#  bb.debug(1, "VAR_A_t212_t21 is %s\n" %var3_t21)
#}
#
#addtask do_key_expansion_one_py
# <--

# -->
VAR_A_t22        = "1_t22"
VAR_B_t22       .= "${VAR_A_t22}"
VAR_C_t22       := "${VAR_A_t22}"
VAR_A_t22       .= "2_t22"
VAR_A_t22:append = "3_t22"

python do_var_exp_one_py () {
   var1_t22 = d.getVar("VAR_A_t22", True)
   var2_t22 = d.getVar("VAR_B_t22", True)
   var3_t22 = d.getVar("VAR_C_t22", True)
   bb.debug(1, "Var expansion one")
   bb.debug(1, "VAR_A_t22 is %s\n" %var1_t22)
   bb.debug(1, "VAR_B_t22 is %s\n" %var2_t22)
   bb.debug(1, "VAR_C_t22 is %s\n" %var3_t22)
}

addtask do_var_exp_one_py
# <--

# -->
VAR_A_t23:append =  "1_t23"
VAR_A_t23:append += "2_t23"
VAR_A_t23:append .= "3_t23"

python do_var_exp_two_py () {
  var1_t23 = d.getVar("VAR_A_t23", True)
  bb.debug(1, "Var expansion two")
  bb.debug(1, "VAR_A_t23 is %s\n" %var1_t23)
}

addtask do_var_exp_two_py
# <--

# -->
VAR_A_t24 = "2_t24"
${VAR_A_t24}:append = "1_t24"

python do_var_exp_three_py () {
  var1_t24 = d.getVar("VAR_A_t24", True)
  var2_t24 = d.getVar("2_t24", True)
  bb.debug(1, "Var expansion three")
  bb.debug(1, "VAR_A_t24 is %s\n" %var1_t24)
  bb.debug(1, "2_t24 is %s\n" %var2_t24)
}

addtask do_var_exp_three_py
# <--
