DESCRIPTION = "elce 2021 bitbake test 3"
SECTION = "examples"
SUMMARY = "bitbake test"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
# _append on unset variable GIFTSt11
GIFTSt11:append = " puzzle"

python do_appnd_on_unset_var_py () {
  var1_GIFTSt11 = d.getVar("GIFTSt11", True)
  bb.warn("GIFTSt11 is %s\n" %var1_GIFTSt11)
}

addtask do_appnd_on_unset_var_py
# do_appnd_on_unset_var_py: GIFTSt11 is  puzzle
# GIFTSt11=" puzzle"
# <--

# -->
GIFTSt12 = "train"
GIFTSt12:append = " puzzle"
# OOPSIES, my nephew wants a doll and not a puzzle
GIFTSt12:append = " doll"

python do_appnd_on_set_var_py () {
  var1_GIFTSt12 = d.getVar("GIFTSt12", True)
  bb.warn("GIFTSt12 is %s\n" %var1_GIFTSt12)
}

addtask do_appnd_on_set_var_py
# do_appnd_on_set_var_py: GIFTSt12 is train puzzle doll
# GIFTSt12="train puzzle doll"
# <--

# -->
GIFTSt13 = "train"
GIFTSt13:append = " puzzle"
# OOPSIES, my nephew wants a doll and not a puzzle
GIFTSt13:append = " doll"
GIFTSt13:remove = "puzzle"

python do_appnd_on_set_var_and_rmove_py () {
  var1_GIFTSt13 = d.getVar("GIFTSt13", True)
  bb.warn("GIFTSt13 is %s\n" %var1_GIFTSt13)
}

addtask do_appnd_on_set_var_and_rmove_py
# do_appnd_on_set_var_and_rmove_py: GIFTSt13 is train  doll
# GIFTSt13="train  doll"
# <--

# --> error in presentation
#     Jester trick does show the sequence in which varibale is modified in file 55 -> 61
#     But it does not show in which sequence operators are actually are applied
#     e.g. _remove at the very end in this example
GIFTSt14 = "train"
GIFTSt14:append = " puzzle"
# OOPSIES, my nephew wants a doll and not a puzzle
GIFTSt14:append = " doll"
GIFTSt14:remove = "puzzle"
# OOPSIES, my nephew actually wants a puzzle too
GIFTSt14:append = " puzzle"

python do_appnd_on_set_var_and_rmove_and_appnd_py () {
  var1_GIFTSt14 = d.getVar("GIFTSt14", True)
  bb.warn("GIFTSt14 is %s\n" %var1_GIFTSt14)
}

addtask do_appnd_on_set_var_and_rmove_and_appnd_py
# do_appnd_on_set_var_and_rmove_and_appnd_py: GIFTSt14 is train  doll
# GIFTSt14="train  doll "
# + bitbake bitbake-test-elce-2021-3 -e
# + awk '/^# \$GIFTSt14 \[/,/^GIFTSt14/'
#  $GIFTSt14 [5 operations]
#    set /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:55
#      "train"
#    _append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:56
#      " puzzle"
#    _append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:58
#      " doll"
#    _remove /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:59
#      "puzzle"
#    _append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:61
#      " puzzle"
#  pre-expansion value:
#    "train puzzle doll puzzle"
# GIFTSt14="train  doll "
#
# Note:
# _remove, another "postponed" operator
#    • Tell Bitbake "resolve ??= ?= = += =+ .= =.
#      then _append/_prepend then _remove"
#    • Final and removes all occurrences!
# <-- error in presentation


# --> 
# _remove, to be avoided (3rd party layer)
# _append/_prepend to be carefully chosen
GIFTSt15 = "train"
GIFTSt15:append = " puzzle"
GIFTSt15:append = " doll"
UNWANTED_GIFTSt15 = "puzzle"
GIFTSt15:remove = "${UNWANTED_GIFTS}"
# Trick to undo _remove
UNWANTED_GIFTSt15 = ""

python do_appnd_on_set_var_and_rmove_and_unwanted_py () {
  var1_GIFTSt15 = d.getVar("GIFTSt15", True)
  var1_UNWANTED_GIFTSt15 = d.getVar("UNWANTED_GIFTSt15", True)
  bb.warn("GIFTSt15 is %s\n" %var1_GIFTSt15)
  bb.warn("UNWANTED_GIFTSt15 is %s\n" %var1_UNWANTED_GIFTSt15)
}

addtask do_appnd_on_set_var_and_rmove_and_unwanted_py
# do_appnd_on_set_var_and_rmove_and_unwanted_py: GIFTSt15 is train puzzle doll
# do_appnd_on_set_var_and_rmove_and_unwanted_py: UNWANTED_GIFTSt15 is 
#
# GIFTSt15="train puzzle doll"
#
# + bitbake bitbake-test-elce-2021-3 -e
# + awk '/^# \$GIFTSt15 \[/,/^GIFTSt15/'
#  $GIFTSt15 [4 operations]
#    set /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:98
#      "train"
#    _append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:99
#      " puzzle"
#    _append /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:100
#      " doll"
#    _remove /workdir/sources/poky-training/meta/recipes-extended/20_bitbake/bitbake-test-elce-2021-3_1.0.0.bb:102
#      "${UNWANTED_GIFTS}"
#  pre-expansion value:
#    "train puzzle doll"
# GIFTSt15="train puzzle doll"
# <--

# -->

# -->
VAR1t16  = "1"
VAR2t16 := "${VAR1t16}"
VAR1t16  = "2"

python do_immediate_and_lazy_py () {
  var1_VAR1t16 = d.getVar("VAR1t16", True)
  bb.warn("VAR1t16 is %s\n" %var1_VAR1t16)

  var1_VAR2t16 = d.getVar("VAR2t16", True)
  bb.warn("VAR2t16 is %s\n" %var1_VAR2t16)
}

addtask do_immediate_and_lazy_py
# do_immediate_and_lazy_py: VAR1t16 is 2
# do_immediate_and_lazy_py: VAR2t16 is 1
# <--

# -->
VAR1t17  = "1"
VAR2t17  = "${VAR1t17}"
VAR1t17  = "2"

python do_lazy_py () {
  var1_VAR1t17 = d.getVar("VAR1t17", True)
  bb.warn("VAR1t17 is %s\n" %var1_VAR1t17)

  var1_VAR2t17 = d.getVar("VAR2t17", True)
  bb.warn("VAR2t17 is %s\n" %var1_VAR2t17)
}

addtask do_lazy_py
# do_lazy_py: VAR1t17 is 2
# do_lazy_py: VAR2t17 is 2
# <--

# --> Explanation
# Now!
#   := immediate expansion operator
# FILESEXTRAPATHS:prepend := "${THISDIR}/lumpi:"
#
# TOPIMMEDIATE := "${THISDIR}"
# TOPLAZY       = "${THISDIR}"
#
# those are in an include in a subdir - (top level) ${THISDIR}/lumpi:
#
# INCIMMEDIATE := "${THISDIR}"
# INCLAZY       = "${THISDIR}"
#
# THISDIR is directory at expansion time
#    Variables expanded after all parsing
#    Called "flattening"
#    If =,  THISDIR is directory of the top level recipe  <-- yes
#           TOPLAZY:         /workdir/sources/meta-bb-syntax/recipes-immediate/immediate 
#           INCLAZY:         /workdir/sources/meta-bb-syntax/recipes-immediate/immediate
#
#    If :=, THISDIR is dir of currently parsed file       <-- yes (but the same on top level)
#           TOPIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate
#           INCIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi
#
# explanation:
# In the top level file it does not make any difference!
# In the included  file it does!
#
# see:
#
# meta-bb-syntax/recipes-immediate/immediate
#
# <-- Explanation

